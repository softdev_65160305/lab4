/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author informatics
 */
public class Player {
    private String symbol;
    private int wincount,losecount,drawcount;

    public Player(String symbol) {
       this.symbol=symbol;
    }
    
public String getSymbol(){
     return symbol;
 }
public int getWincount(){
    return wincount;
}
public int getLosecount(){
    return losecount;
}
public int drawcount(){
    return drawcount;
}
public void win(){
    wincount++;
}
public void lose(){
    losecount++;
}
public void draw(){
    drawcount++;
}
public void resetwin(){
    wincount=0;
}
public void resetlose(){
    losecount=0;
}
public void resetdraw(){
    drawcount=0;
}
}
