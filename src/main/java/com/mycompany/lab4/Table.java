/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author informatics
 */
public class Table {
    private String table[][]={{"1","2","3"},{"4","5","6"},{"7","8","9"}};
    private Player currentplayer;
    private Player player1;
    private Player player2;
    private int turncount=0;
    private String number;
    
    public Table(Player player1,Player player2){
        this.player1=player1;
        this.player2=player2;
        this.currentplayer=player1;
    }
    public String[][] getTable (){
        return table;
    }
    public boolean checkwin(){
        //horizontal
        if(table[0][0].equals(currentplayer.getSymbol()) && table[0][1].equals(currentplayer.getSymbol()) && table[0][2].equals(currentplayer.getSymbol())){
            return true;
        }else if(table[1][0].equals(currentplayer.getSymbol()) && table[1][1] .equals(currentplayer.getSymbol()) && table[1][2].equals(currentplayer.getSymbol())){
            return true;
        }else if(table[2][0].equals(currentplayer.getSymbol()) && table[2][1] .equals(currentplayer.getSymbol()) && table[2][2].equals(currentplayer.getSymbol())){
            return true;
            //vertical
        }else if(table[0][0].equals(currentplayer.getSymbol()) && table[1][0].equals(currentplayer.getSymbol()) && table[2][0].equals(currentplayer.getSymbol())){
            return true;
        }else if(table[0][1] .equals(currentplayer.getSymbol()) && table[1][1].equals(currentplayer.getSymbol()) && table[2][1].equals(currentplayer.getSymbol())){
            return true;
        }else if(table[0][2] .equals(currentplayer.getSymbol()) && table[1][2].equals(currentplayer.getSymbol()) && table[2][2].equals(currentplayer.getSymbol())){
            return true;
            //Diagonal
        }else if(table[0][0].equals(currentplayer.getSymbol()) && table[1][1].equals(currentplayer.getSymbol()) && table[2][2].equals(currentplayer.getSymbol())){
            return true;
        }else if(table[0][2].equals(currentplayer.getSymbol()) && table[1][1].equals(currentplayer.getSymbol())&& table[2][0].equals(currentplayer.getSymbol())){
            return true;
        }
        return false;
    }
    public boolean checkdraw(){
        if (table[0][0].equals("1") || table[0][1].equals("2")  || table[0][2].equals("3")  || table[1][0].equals("4")  || table[1][1].equals("5")  || table[1][2].equals("6")  || table[2][0].equals("7")  || table[2][1].equals("8")  || table[2][2].equals("9")) {
            return false;
        }
        return true;
    }
    public Player switchPlayer(){
        if(currentplayer == player1){
        currentplayer = player2;
        }else{
        currentplayer = player1;
        }   
        return currentplayer;
    }  
    public boolean settable(String number){
       if(number.equals("1")){
                table[0][0] = currentplayer.getSymbol();
            } else if (number.equals("2")) {
                table[0][1]=currentplayer.getSymbol();
            } else if (number.equals("3")) {
                table[0][2]=currentplayer.getSymbol();
            } else if (number.equals("4")) {
                table[1][0]=currentplayer.getSymbol();
            } else if (number.equals("5")) {
                table[1][1]=currentplayer.getSymbol();
            } else if (number.equals("6")) {
                table[1][2]=currentplayer.getSymbol();
            } else if (number.equals("7")) {
                table[2][0]=currentplayer.getSymbol();
            } else if (number.equals("8")) {
                table[2][1]=currentplayer.getSymbol();
            } else if (number.equals("9")) {
                table[2][2]=currentplayer.getSymbol();
            }
            return false;
    }
    public Player getCurrentPlayer(){
        return currentplayer;
    }
    public void Draw(){
        System.out.println("!!!Draw!!!");
    }
     public void isWin(){
        System.out.println(currentplayer.getSymbol() + " !!!Winner!!!");
    }

}
