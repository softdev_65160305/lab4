/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
/**
 *
 * @author informatics
 */
public class Game {
    static Set<String> chosenNumber = new HashSet<>();
    private Player player1,player2;
    private Table table;
    private boolean gameOver =false;
    private String number ;
    
    public Game(){
    player1 = new Player("X");
    player2 = new Player("O");
    table = new Table(player1,player2);
}    
public void play(){
    showwelcome();
    while(true){
        gameOver=false ;
        showtable();
        showturn();
        inputNumber();
        if(table.checkwin()==true){
            showtable();
            table.isWin();
            gameOver=true;
            savestat();
            showstatplayer();
        }
        if(table.checkdraw()==true){        
            table.Draw();
            gameOver=true;
            savestat();
            showstatplayer();
        }
        if(gameOver=true){
            if(startnewgame().equalsIgnoreCase("Y")){
                newGame();
                continue;
            }else{
                break;
            }
            }
        table.switchPlayer();
}
}
private void showwelcome(){
    System.out.println(" Welcome To XO ");
}
private void showtable(){
    String[][] table =new String[3][3];
            table[0][0] = "1";
        table[0][1] = "2";
        table[0][2] = "3";
        table[1][0] = "4";
        table[1][1] = "5";
        table[1][2] = "6";
        table[2][0] = "7";
        table[2][1] = "8";
        table[2][2] = "9";
    for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
            System.out.print(table[i][j]+" ");
        }
        System.out.println();
    }
    }
private void newGame(){
        chosenNumber.clear();
        Table table = new Table(player1,player2);  
}
private void showturn(){
    System.out.println(table.getCurrentPlayer().getSymbol() +" Turn ");
}
public void inputNumber(){
        Scanner kb = new Scanner(System.in);
        System.out.print("Plase input your Number: ");
        number = kb.next();
        checkRepeat();
        table.settable(number);    
}
private String startnewgame(){
    System.out.print("Do you want play again? (Y/N) :");
    Scanner kb  =new Scanner(System.in);
    String playAgain = kb.nextLine();
    return playAgain;
}
private void savestat(){
    if(table.getCurrentPlayer().getSymbol()=="X"){
        player1.win();
        player2.lose();
    }else if(table.getCurrentPlayer().getSymbol()=="O"){
        player1.lose();
        player2.win();
    }else{
        player1.draw();
        player2.draw();
    }
}
private void showstatplayer(){
    System.out.println("______________________________________");
    System.out.println("X win point :"+player1.getWincount()+"/");
    System.out.println("X lose point"+player1.getLosecount()+"/");
    System.out.println("X draw point"+player1.drawcount()+"/");
    System.out.println("______________________________________");
    System.out.println("O win point :"+player2.getWincount()+"/");
    System.out.println("O lose point"+player2.getLosecount()+"/");
    System.out.println("O draw point"+player2.drawcount()+"/");
    System.out.println("______________________________________");
}
 private String checkRepeat(){
    while (chosenNumber.contains(number)) {
            showtable();
            showturn();
            inputNumber();
            }
        chosenNumber.add(number);
        return number;     
    }

}

